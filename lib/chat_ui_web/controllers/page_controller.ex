defmodule ChatUiWeb.PageController do
  use ChatUiWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
